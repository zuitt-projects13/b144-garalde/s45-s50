import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col} from 'react-bootstrap'
import { useParams, useHistory, Link } from 'react-router-dom'
//import course id using url

import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function CourseView() {

	//usecontext to share values between components
	const {user} =useContext(UserContext)
	const history = useHistory()		

	//the useParams hook allows us to retrieve the courseId passed via URL
	const { courseId } = useParams();

	//enroll function to enroll a user to a specific course
	const enroll = (courseId) => {
		fetch('http://localhost:4000/api/users/enroll',{
			method:'POST',
			headers: {
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId:courseId
			})
		})
		.then(response => response.json())
		.then(data => {
		    console.log(`enroll: ${data}`);
		    console.log(`courseId: ${courseId}`);

		    if(data === true) {
		    	Swal.fire({
		    		title: "Successfully enrolled.",
		    		icon:"success",
		    		text:"You have Successfully enrolled for this course."
		    	})

		    	history.push("/courses")
		    }
		    else {
		    	Swal.fire({
		    		title: "Something went wrong",
		    		icon:"error",
		    		text:"Please try again."
		    	})
		    	
		    }

		})
	}


	//the use Effect is used to check if the courseId is retrieved properly
	useEffect(() => {
		console.log(courseId)

		fetch(`http://localhost:4000/api/courses/${courseId}`)
		.then(response => response.json())
		.then(data => {
		    console.log(`coursedetails: ${data}`);

		    setName(data.name);
		    setDescription(data.description);
		    setPrice(data.price);
		})

	},[courseId]);

	const [name,setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="cardHighlight p-3 mb-2">
					  <Card.Body className="text-center">
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text>
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>PhP {price}</Card.Text>
					    <Card.Subtitle>Class Schedule:</Card.Subtitle>
					    <Card.Text>8:00am - 5:00pm</Card.Text>
					    { (user.id !== null) ?
							<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
						:
							<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>

					    }
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}