// import state hook from react
// import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { Row, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {
	// checks to see if the data was successflly passed
	// console.log(props)
	// console.log(typeof props)

	// CourseCard(props)
	// props.courseProp.name

	// CourseCard({courseProp})
	// {courseProp.name}

	const { _id, name, description, price } = courseProp

	// use the state hook in this component to be able to store its state 
	// states are used to keep track information related to individual components
	// props if you dont plan to update the data. READ ONLY
	// state you can change the value

	/*
	Syntax:
		const [getter, setter] = useState(initialGetterValue)
		getter is the initial value
		setter is the modifier of the getter

	*/
	//use 'set' to say that this is the setter
	//the state hook will return an array
	
	// const [count, setCount] = useState(0);
 //    const [seats, setSeats] = useState(30);
 //    const [isOpen, setIsOpen] =useState(false)
 //    console.log(useState(0));


	//function to keep track of the enrollees for a course
/*    function enroll(){
        setCount(count + 1);
        console.log('Enrollees: ' + count);
        setSeats(seats - 1);
        console.log('Seats: ' + seats);
    }*/


	//Define a useEffect hook to have CourseCard component perform a certain task after every DOM update
	// useEffect(() => {
	// 	if (seats === 0) {
	// 		setIsOpen(true)
	// 	}
	// })

	return (
		<Card className="cardHighlight p-3 mb-2">
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description:</Card.Subtitle>
		    <Card.Text>{description}</Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>PhP {price}</Card.Text>
		    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
	)
}