//Import the fragment component from react
// import { Fragment } from 'react'
import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound'
import { useState, useEffect} from 'react'
import './App.css';
import { UserProvider } from './UserContext'     



function App() {
  //State hook for the user state that is defined for a global scope
  //initialize an object the properties from the localStorage

  const [user, setUser] = useState({
    // email:localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  //function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  //used to check if the user information is properly stored upon login and the login storage information is cleared upon logout
  useEffect(() => {
    console.log(user)
    console.log(localStorage)

  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path ="/" component={Home} />
            <Route exact path ="/courses" component={Courses} />
            <Route exact path ="/courses/:courseId" component={CourseView} />
            <Route exact path ="/login" component={Login}/>
            <Route exact path ="/register" component={Register} />
            <Route exact path ="/logout" component={Logout} />
            <Route component={NotFound} />
          </Switch>
        </Container>

      </Router>

    </UserProvider>


  );
}

export default App;
