import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import courses from '../data/Courses'

export default function Courses() {
	// checks to see if mock data was captured
	// console.log(courses)
	// console.log(courses[0])
	// <CourseCard courseProp={courses[0]}/>

	//State that will be used to store the courses retrieve from the database
	const [courses, setCourses] = useState([]);

	//Retrieve the courses from the database upon initial render of the "courses" component

	useEffect(() => {
		fetch("http://localhost:4000/api/courses/all")
		.then(response => response.json())
		.then(data => {
			console.log(data)
			//set the "courses" state to map the data retrieve from the fetch request into several "CourseCard" component
			setCourses(
				 	 	data.map(course => {
						return(
							<CourseCard key={course._id} courseProp={course}/>
						);
				})
			)

		})
	}, []);

	/*const coursesData = courses.map(courses => {
		return(
			<CourseCard key={courses.id} courseProp={courses}/>
		)
	})*/
	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}