//this will clear the localStorage after logout
import { Redirect,  } from 'react-router-dom'
import UserContext from '../UserContext'   
import { useContext, useEffect } from 'react';

export default function Logout(){
	// localStorage.clear();

	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser()

	useEffect(() => {
		//set the user state back to its original value
		setUser({id:null})
	})

	// Redirect back to login
	return(
		<Redirect to='/login' />
	)
}
