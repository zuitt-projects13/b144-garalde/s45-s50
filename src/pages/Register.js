import { useState, useEffect, useContext } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import UserContext from '../UserContext' 
import { Redirect, useHistory} from 'react-router-dom' 
import Swal from 'sweetalert2'

export default function Register(){
	//allows to consume the user context object and its properties to use for user validation
	const {user} = useContext(UserContext)
	const history = useHistory()	

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const [isRegistered, setIsRegistered] = useState(false);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);	
	console.log(lastName);	

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();
		

		fetch('http://localhost:4000/api/users/checkEmail',{
			method:'POST',
			headers: {
				"Content-Type":"application/json",
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(response => response.json())
		.then(data => {
		    
		    if(data === true) {
		    	Swal.fire({
		    		title: "Email is already registered",
		    		icon:"error",
		    		text:"Please try again."
		    	})

		    }
		    else {

		    	fetch('http://localhost:4000/api/users/register',{
		    		method:'POST',
		    		headers: {
		    			"Content-Type":"application/json",
		    		},
		    		body: JSON.stringify({
		    			firstName: firstName,
		    			lastName: lastName,
		    			age: age,
		    			gender: gender,
		    			email: email,
		    			// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		    			password: password2,
		    			mobileNo: mobileNo
		    		})
		    	})
		    	.then(response => response.json())
		    	.then(data => {


					// Clear input fields
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setAge('');
					setGender('');
					setMobileNo('');

					alert('Thank you for registering!')

					history.push("/login")	

				})
		    	
		    }

		})





	}

	useEffect(() => {
		// Validation to enable the submit buttion when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && age !== '' && gender !== '' && mobileNo !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, age, gender,mobileNo])

	return (
		(user.id !== null) ?
            <Redirect to="/courses" />
        :


		<Container>
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="firstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter first name" 
			    	value = {firstName}
			    	onChange = { e => setFirstName(e.target.value)}
			    	required 
			    />
			   </Form.Group>


			   <Form.Group className="mb-3" controlId="lastName">
			     <Form.Label>Last Name</Form.Label>
			     <Form.Control 
			     	type="text" 
			     	placeholder="Enter Last name" 
			     	value = {lastName}
			     	onChange = { e => setLastName(e.target.value)}
			     	required 
			     />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="age">
			      <Form.Label>Age</Form.Label>
			      <Form.Control 
			      	type="text" 
			      	placeholder="Enter age" 
			      	value = {age}
			      	onChange = { e => setAge(e.target.value)}
			      	required 
			      />
			     </Form.Group>

			     <Form.Group className="mb-3" controlId="gender">
			       <Form.Label>Gender</Form.Label>
			       <Form.Control 
			       	type="text" 
			       	placeholder="Enter gender" 
			       	value = {gender}
			       	onChange = { e => setGender(e.target.value)}
			       	required 
			       />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter mobile number (11 digits)" 
			        	value = {mobileNo}
			        	onChange = { e => setMobileNo(e.target.value)}
			        	required 
			        />
			       </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			{/* Conditionally render submit button based on isActive state */}
			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			  
			</Form>
		</Container>
	)
}
