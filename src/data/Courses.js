const courseData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "PHP Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price:45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phyton - Django",
		description: "Phyton Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price:50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Laravel",
		description: "Java Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price:55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "HTML Introduction",
		description: "HTML Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		price:60000,
		onOffer: true
	}
];

export default courseData;