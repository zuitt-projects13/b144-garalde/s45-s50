import React from 'react'

//set global scope

const UserContext = React.createContext();

//the "Provider" component allows other components to consume/use the context object and supply the neccesary information needed to the context object


export const UserProvider = UserContext.Provider;

export default UserContext;